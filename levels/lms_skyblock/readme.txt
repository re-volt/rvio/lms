============================================
                             LMS Skyblock
		         by Zeino
============================================
Made using Mineways program and Blender. 
Functioning LMS arena taking the use of the newest 19.1230a RVGL version, 
with random pickup placements everytime.
Can be played in older versions too, doing that, game will use all 8 pickups.
Original skyblock with pickups replaced by 2D diamonds! Credits for skybox by Tubers.
Music is Cat by C418.
Have fun and enjoy!
============================================