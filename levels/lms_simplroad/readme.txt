﻿                    +-------------------------****--------------------------+
====================¦	              LMS Road of Simplicity	            ¦====================
##||#####||#####||##¦    "Last Man Standing" game mode arena for Re-Volt    ¦##||#####||#####||##
====================¦                 By VaidX47 (VaiDuX461)                ¦====================
                    +-------------------------****--------------------------+
More tracks by me: http://revoltzone.net/search.php?term=VaiDuX461&db3=on
For updates and other stuff, check: www.mediafire.com/?dbg9nyxxnwc16

                                          /-*********-\
-========================================- Information -========================================-

Name: LMS Road of Simplicity
Release date: 2013-05-05
Folder name: lms_simplroad
Track Type: Last Man Standing (Online only)
Difficulty: Extreme
Number of Stars: 2
Custom Features: Yes (HUD and sounds)
Bonus Features: No

Latest RVGL patch is recommended to play this track, but should also run fine on old versions, including 1.1.
Custom track features work since v1.2 Alpha 11.1215. 1.2 users will need to replace font (read installation).

                                          /-*********-\
-========================================- Description -========================================-

It's a battle arena for the unofficial "Last Man Standing" game mode, based on my "Road of Simplicity 1" track.
The layout is rather tricky and it is one of the more difficult arenas available.

In the "Last Man Standing", you must knock out other players. Last survivor on platform wins.

Rules:
 -Don't fall off the platform
 -Get the other player off the platform with pick-ups

                                         /-**********-\
-=======================================- Installation -========================================-

Just extract two folders (levels, gfx) to the main Re-Volt directory.
Default directory: "C:\Program Files\Acclaim Entertainment\Re-Volt".

The arena includes some custom features, like custom sounds and graphics.
They can be deleted or renamed, see folder "custom" inside "\levels\lms_simplroad".
It is suggested to keep them, since there's a special darkened font, so it's easier to read in-game.
Anyway, you can still delete sounds if they annoy you for some reason.

If you are on 1.2 or old RVGL version, make sure to copy fonts from ".\custom\legacy font" folder and
replace the existent ones found in ".\custom".

                                        /-************-\
-======================================- Uninstallation -======================================-

Delete "lms_simplroad.bmp" file inside "gfx" folder and "lms_simplroad" inside "levels".

You can also use WolfR4 (http://jigebren.free.fr/games/pc/re-volt/wolfr4/) to delete tracks.

                                          /-********-\
-========================================- Change log -=========================================-
Note: The arena won't reach its final versioning number until RVGL natively implements LMS game mode.

v0.82 [2020-09-20]:
 -Implemented anti-reposition measures. Players will be repositioned below the platform after using reposition.
  (Thanks to "The B!" for doing .fan and .taz files)
 -Redone font textures for RVGL compatibility (old files can still be found in "legacy font" folder)
 -Replaced "menuNext.wav" audio file (decreased sampling rate and slowed it down, since RVGL played it too fast)
 -Fixed ENVSTILL texture path (apparently it was set to be loaded from simplroad1 folder)

v0.81 [2018-04-28]:
 -RVGL shader render fix (updated .w file with unmapped instances)
 -Added `DIFFICULTY` parameter, it will now show "Extreme" in the track selection screen (since RVGL 17.1009a build)
  *I'm still unsure about the difficulty, for now it's going to be Extreme.
 -Changed in-game name. Uses `LMS` as a prefix, so it's "LMS Last Man Standing" instead of "Last Man Standing LMS".
 
v0.8  [2013-05-05]:
 -First release

                                         /-**********-\
-=======================================- Known issues =========================================-
Known bugs which are impossible or too difficult to fix for me.															   

 -Minor "pixel width holes" on the platform (not unusual with prm kit based tracks).
 -Bomb explosion "yellow leftovers" might clip on the ground, also minor.
 -Car skidmarks created on the bottom can be seen in certain positions through the normal white platform.

                                            /-***-\
-==========================================- To do -============================================-

 -None at the moment.

                                     /-******************-\
-===================================- Used Tools and Media -====================================-

MAKEITGOOD Editors, wall prm kit, rvglue, prm GUI, prm2ncp.
Various sounds from old video games: 1943, F1 Race, Mega Man 3, Mega Man 4, Mario Bros, Super Mario Bros,
Super Mario Bros 2, Super Mario Bros 3 and Pong.

                                           /-*****-\
-=========================================- Credits -==========================================-

Track design, layout, building and texture editing
	VaidX47 (formerly VaiDuX461)

Wall PRM kit
	Unknown

Sounds
	Capcom (Mega Man series and 1943)
	Nintendo (Mario series and F1 Race)
	Atari (Pong)
	
rvtmod7 (rvglue)
	Gabor Varga, Alexander Kroller, Christer Sundin

prm and prm GUI
	kay
	
Original game
	Acclaim Studios London & Acclaim Entertainment

Hardware which was used to make this
	"Sh*tty computer"

Readme design
	VaidX47

Special thanks
	RV Team (Re-Volt 1.2 and RVGL)
	The B!

	    Thanks to YOU for downloading and everyone else from the Re-Volt community

                                             /-***-\
-===========================================- Links -===========================================-

http://rvgl.re-volt.io                    - RVGL patch (formerly v1.2) home page
http://re-volt.io                         - Game news, downloads, documentation and more
http://re-volt.io/discord                 - Re-Volt related Discord servers (The most active in English is called Re-Volt)
http://revoltzone.net                     - Site for downloading additional custom content for Re-Volt
http://www.revoltxtg.co.uk                - Other place for game content downloads
http://forum.re-volt.io                   - The current forum of Re-Volt and RVGL project

                                          /-*********-\
-========================================- Permissions -========================================-

You can redistribute this archive in any electronic format WITHOUT making any modifications.
If the author can be mentioned before downloading this archive, please use their name and no one else.
IT MUST NOT TO BE SOLD FOR ANY COMMERCIAL PURPOSES!

Editing AND redistributing is only allowed when you contact me and ask for my permission first.
I can be contacted on Discord through RV server, Re-Volt forums (preferably The Re-Volt Hideout) and 
email: vaidux461@gmail.com
If I won't respond anywhere in 6 months, that means you are free to do it.
Removing authors and claiming the work as your own is still strictly FORBIDDEN.

If something was used or inspired from this archive, please mention original authors in your work.
Blatantly copying things, such as: track layout and name IS FORBIDDEN. Always contact me if you are unsure.

_________________________________________«««===~&~===»»»_________________________________________
Excuse me if there are any grammar errors
Have fun!
©2013, 2018, 2020 VaidX47
vaidux461@gmail.com				   
Track ver 0.82