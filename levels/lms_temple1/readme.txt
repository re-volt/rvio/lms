Last Man Standing: Temple 1 (Version 1.0)
Original track by marv

Edited by Paperman for release
New system added

Feel free to share!
This is a multiplayer map. Get your opponents off the platform!

Watch out with lava!! Raises after 1 minute!!!

RVGL is required to play this. Get it at https://re-volt.io/downloads/game

Author: Paperman, Marv
2023-11-12
https://re-volt.io

Textures by Skitch (Images of Gizah)

3-May-2013 (Original Preview Track)