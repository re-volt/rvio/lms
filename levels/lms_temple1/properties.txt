
MATERIAL {
  ID              10                            ; Material ID [0 - 63]
  Name            "ICE1"                        ; Display name
  Color           184 255 242                   ; Display color

  Skid            true                          ; Skidmarks appear on material
  Spark           true                          ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      false                         ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           false                         ; Material emits dust

  Roughness       0.300000                      ; Roughness of the material
  Grip            0.300000                      ; Grip of the material
  Hardness        0.200000                      ; Hardness of the material

  DefaultSound    -1                            ; Sound when driving
  SkidSound       7                             ; Sound when skidding
  ScrapeSound     5                             ; Car body scrape [5:Normal]

  SkidColor       48 48 48                      ; Color of the skidmarks
  CorrugationType 0                             ; Type of bumpiness
  DustType        0                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}
