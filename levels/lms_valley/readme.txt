============================================
                             LMS Valley
		         by Zeino
============================================
Made out of JimK offroad instances kit, remodelled and reshaded with the help of Kiwi!
Functioning LMS arena taking the use of the newest 19.1230a RVGL version, with random pickup placements everytime.
Can be played in older versions too, doing that, game will use all 10 pickups.
Created in a matter of 3 days, with much help from community, finally completed 31st December 2019.
Beta tested thanks to hajducsekb, with DVark09, SS06, URV, hajducsekb and me playing.
============================================
Update 1/1/2020
----------------------------------------------------------------------
Huge thank you goes to Instant who was kind enough to help me out with the many collision issues and such.
Also thanks to him, the track is better in many ways, its faster and better optimized I might say haha!
I can't grateful enough! ~
============================================
Have fun and enjoy!
============================================