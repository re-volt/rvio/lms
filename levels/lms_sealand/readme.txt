LMS Sealand by Zeino (Tryxn on RVZ)

hi there, this is a recreation of the micronation of Sealand. this lms arena is a bit bigger than most of the arenas you might come across but i still think its good enough, if you fall on the lower deck you are OUT.
you can read more about it here: https://sealandgov.org/about/
please note that this is not a 1:1 recreation, but i guess its the idea that counts haha
i hope you'll enjoy!

credits: 
the song is skyline.mp3 from the io music pack i guess, i took it from another lms track lol
textures: various sources, internet, LMS: Toytanic, most edited by me
skybox taken from Marv's skybox pack

Special thanks:
my friends At Home for keeping me going, Kiwi