LMS Pit of Doom by Zeino (Tryxn on RVZ)

hey! this is my entry for the rvw autumn party 2023. the lms arena is in a pit of doom, its small and i hope it is just as fun! it has a built in antirepo system, have fun!

credits:
music is Tear their fate by DavidKBD (https://davidkbd.itch.io/purgatory-vol-2-extreme-metal-music-pack)
screaming guy (https://freesound.org/people/NachtmahrTV/sounds/556706/)

Special thanks:
URV, Tubers, OhNej and my other friends At home.