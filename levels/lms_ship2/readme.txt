********************************************************************************
**                               Changelog                                    **
********************************************************************************
--------------------------------------------------------------------------------
Version 23.1106a

- Added Collisions at Roof (EstebanMz)

--------------------------------------------------------------------------------
Version 20.0919a

- First Public release

Fixes (Santiii727 & The B!):
- Adding the original release ROCK movement from LMS Toytanic 1 (2017) to this arena.
- Implemented anti-reposition measures. Players will be repositioned below the platform after using reposition.

Addons (Santiii727):
- Added gfx track to the actual arena.

Fixes (Javildesign):
- .w adjustment for extending .fob usage (.fob visual limited by world size)
- .ncp clean up (reduced poly count)

Addons (Javildesign):
- Added custom animation clouds.m (seamless fly over)
- Added texture (pixabay.com) and made them seamless h/v
- Added properties.txt (copied from rooftop chase redux)
- Added AI and Trackzones for enabling anti cheat repo.
- Added vertex shading to the cliff and platform
- Added rain (.fob)
- Past changelogs from LMS Toytanic 1

Changes (Javildesign):
- Removed skybox from .fob
- Deleted .bmp files from custom folder

--------------------------------------------------------------------------------
Name: LMS Toytanic 2 
Release date: September 2020
Track Type: Last Man Standing (Multiplayer only)
Difficulty: Extreme
--------------------------------------------------------------------------------
Note: Base arena is originally made by Marv from LMS Toytanic 1.

You need RVGL to play this: https://rvgl.re-volt.io/

-Marv, Javildesign, Santiii727

re-volt.io

©️ 2020 Marv and RVGL I/O Community.
