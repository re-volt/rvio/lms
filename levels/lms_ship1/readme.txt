********************************************************************************
**                               Changelog                                    **
********************************************************************************
--------------------------------------------------------------------------------
Version 23.1106a

- Added Collisions at Roof (EstebanMz)

--------------------------------------------------------------------------------
Version 20.0919a

- #1 Update

Fixes (Santiii727 & The B!):
- Transfer the ROCK movements from LMS Toytanic 2 to LMS Toytanic 1 and reverting the original release ROCK movement from this arena to LMS Toytanic 2.
- Changed the old gfx track to the actual arena.
- Implemented anti-reposition measures. Players will be repositioned below the platform after using reposition.
  (Thanks to "The B!" for doing .fan and .taz files)

Fixes (Javildesign):
- .w adjustment for extending .fob usage (.fob visual limited by world size)
- .ncp clean up (reduced poly count)

Addons (Javildesign):
- Added custom animation clouds.m (seamless fly over)
- Added texture (pixabay.com) and made them seamless h/v
- Added properties.txt (copied from rooftop chase redux)
- Added AI and Trackzones for enabling anti cheat repo.
- Added vertex shading to the cliff and platform

Changes (Javildesign):
- Removed skybox from .fob
- Deleted .bmp files from custom folder

--------------------------------------------------------------------------------
Version 20.0509a

- First Public release

Fixes: 
- Crazy ROCK inf movements (Santiii727)

Addons:
- Ocean effects (Javildesign)

--------------------------------------------------------------------------------

Version 17.1022a

- First Beta release (All's Fair III)

--------------------------------------------------------------------------------
Name: LMS Toytanic 1 
Release date: October 2017
Track Type: Last Man Standing (Multiplayer only)
Difficulty: Expert
--------------------------------------------------------------------------------
Note: Marv makes some LMS Arenas in preparation for All's Fair III.

You need RVGL to play this: https://rvgl.re-volt.io/

-Marv, Javildesign, Santiii727, EstebanMz, The B!

re-volt.io

©️ 2017-2023 Marv and RVGL I/O Community.
