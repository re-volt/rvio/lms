Track: LMS On Ice!
Author: rodik
Game Mod: LMS
Tools: Blender 2.79b, Paint 3D, MIG.
Some Textures from Xarc's Snowland 1 track. 
Also many thanks to Xarc for helping with the snow animation.
Read more on the website.