Last Man Standing: Temple 2

-change log-

24.1220 - Fixed NCP.
-------------------------------------------------------------------------

24.0620 - The pyramid has become more slippery
-------------------------------------------------------------------------
23.1116 Version Changelog

Added New liquid animation + Changed GFX (Paperman)
-------------------------------------------------------------------------

20.0918 Version Changelog

Added repo outside arena platform (Credits: The B!)
-------------------------------------------------------------------------
Feel free to share!
This is a multiplayer map. Get your opponents off the platform!

RVGL is required to play this. Get it at https://re-volt.io/downloads/game

Author: Marv
2016-10-25 (Original Release)

http://re-volt.io

Textures by Skitch (Images of Gizah)
